<?php

/**
 * @file
 * Allows administrators to add previous/next pagers to any node type.
 */


/**
 * Implements hook_theme()
 *
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function loft49mod_theme() {
  return array(
    'loft49mod' => array(
      'variables' => array(
        'list' => array(),
      ),
      'template' => 'loft49mod',
    ),
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function loft49mod_form_node_type_form_alter(&$form, $form_state) {
  if (isset($form['type'])) {
    $form['loft49mod'] = array(
      '#type' => 'fieldset',
      '#title' => t('loft49mod settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
    );

    $form['loft49mod']['loft49mod'] = array(
      '#type' => 'checkbox',
      '#title' => t('Build a pager for this content type'),
      '#default_value' => isset($form['loft49mod']['loft49mod']) ? $form['loft49mod']['loft49mod'] : variable_get('loft49mod_' . $form['#node_type']->type, FALSE),
    );

    $form['loft49mod']['loft49mod_head'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add semantic previous and next links to the document HEAD'),
      '#default_value' => isset($form['loft49mod']['loft49mod_head']) ? $form['loft49mod']['loft49mod_head'] : variable_get('loft49mod_head_' . $form['#node_type']->type, FALSE),
      '#states' => array(
        'visible' => array(   // action to take.
          ':input[name=loft49mod]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['loft49mod']['loft49mod_label_type'] = array(
      '#type' => 'select',
      '#title' => t('Pager label type'),
      '#options' => array(
        0 => t('Custom text'),
        1 => t('Node title'),
        2 => t('Node ID'),
      ),
      '#default_value' => isset($form['loft49mod']['loft49mod_label_type']) ? $form['loft49mod']['loft49mod_label_type'] : variable_get('loft49mod_label_type_' . $form['#node_type']->type, 0),
      '#description' => t('Select label type to display'),
      '#states' => array(
        'visible' => array(
          ':input[name=loft49mod]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['loft49mod']['loft49mod_prev_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label for "Previous" link'),
      '#size' => 32,
      '#default_value' => isset($form['loft49mod']['loft49mod_prev_label']) ? $form['loft49mod']['loft49mod_prev_label'] : variable_get('loft49mod_prev_label_' . $form['#node_type']->type, 'Previous'),
      '#states' => array(
        'visible' => array(
          ':input[name=loft49mod]' => array('checked' => TRUE),
          ':input[name=loft49mod_label_type]' => array('value' => 0),
        ),
      ),
    );

    $form['loft49mod']['loft49mod_next_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label for "Next" link'),
      '#size' => 32,
      '#default_value' => isset($form['loft49mod']['loft49mod_next_label']) ? $form['loft49mod']['loft49mod_next_label'] : variable_get('loft49mod_next_label_' . $form['#node_type']->type, 'Next'),
      '#states' => array(
        'visible' => array(
          ':input[name=loft49mod]' => array('checked' => TRUE),
          ':input[name=loft49mod_label_type]' => array('value' => 0),
        ),
      ),
    );

    $form['loft49mod']['loft49mod_firstlast'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show first/last links'),
      '#default_value' => isset($form['loft49mod']['loft49mod_firstlast']) ? $form['loft49mod']['loft49mod_firstlast'] : variable_get('loft49mod_firstlast_' . $form['#node_type']->type, FALSE),
      '#states' => array(
        'visible' => array(
          ':input[name=loft49mod]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['loft49mod']['loft49mod_first_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label for "First" link'),
      '#size' => 32,
      '#default_value' => isset($form['loft49mod']['loft49mod_first_label']) ? $form['loft49mod']['loft49mod_first_label'] : variable_get('loft49mod_first_label_' . $form['#node_type']->type, 'First'),
      '#states' => array(
        'visible' => array(
          ':input[name=loft49mod_firstlast]' => array('checked' => TRUE),
          ':input[name=loft49mod]' => array('checked' => TRUE),

        ),
      ),
    );

    $form['loft49mod']['loft49mod_last_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label for "Last" link'),
      '#size' => 32,
      '#default_value' => isset($form['loft49mod']['loft49mod_last_label']) ? $form['loft49mod']['loft49mod_last_label'] : variable_get('loft49mod_last_label_' . $form['#node_type']->type, 'Last'),
      '#states' => array(
        'visible' => array(
          ':input[name=loft49mod_firstlast]' => array('checked' => TRUE),
          ':input[name=loft49mod]' => array('checked' => TRUE),
        ),
      ),
    );

  }
}

/**
 * Implements hook_field_extra_fields().
 */
function loft49mod_field_extra_fields() {
  $extra = array();
  foreach (node_type_get_names() as $type => $name) {
    if (variable_get('loft49mod_'. $type, NULL)) {
      $extra['node'][$type] = array(
        'display' => array(
          'loft49mod_pager' => array(
            'label' => t('Pager'), 
            'description' => t('loft49mod module content pager.'), 
            'weight' => 5,
          ), 
        ),
      );
    }
  }
  return $extra;
}

/**
 * Implements hook_node_view().
 */
function loft49mod_node_view($node, $view_mode = 'full') {
  if (node_is_page($node) && variable_get('loft49mod_'. $node->type, NULL)) {
    $node->content['loft49mod_pager'] = array(
      '#theme' => 'loft49mod',
      '#list' => loft49mod_build_list($node),
    );
    
    if (variable_get('loft49mod_head_' . $node->type, NULL)) {
      $links = loft49mod_build_list($node);
      if (!empty($links['prev'])) {
        drupal_add_html_head_link(array(
          'rel' => 'prev',
          'href' => url('node/' . $links['prev']['nid']),
        ));
      }
      if (!empty($links['next'])) {
        drupal_add_html_head_link(array(
          'rel' => 'next',
          'href' => url('node/' . $links['next']['nid']),
        ));
      }
    }
  }
}

/**
 *  Function that builds the list of nodes
 */
function loft49mod_build_list($node) {
  $master_list = &drupal_static(__FUNCTION__);
  if (!isset($master_list)) {
    $master_list = array();
  }
  if (!isset($master_list[$node->nid])) {
    // Create a starting-point query object
    $query = db_select('node')
      ->fields('node', array('nid', 'title'))
      ->condition('nid', $node->nid, '!=')
      ->condition('status', 1)
      ->condition('type', $node->type, '=')
      ->range(0, 1);
    $query->join('field_revision_field_event_date', 'frfed', 'frfed.entity_id = node.nid');
    $query->fields('frfed', array('field_event_date_value'));

    $first = clone $query;
    $list['first'] = $first
//      ->condition('created', $node->created, '<=')
//      ->orderBy('created', 'ASC')
      ->condition('frfed.field_event_date_value', $node->field_event_date['und'][0]['value'], '<=')
      ->orderBy('frfed.field_event_date_value', 'ASC')
      ->execute()->fetchAssoc();
    
    loft49mod_extract_date_field($list, 'first');

    $list['current'] = array(
      'nid' => $node->nid,
      'title' => $node->title,
      'field_event_date_value'=>$node->field_event_date['und'][0]['value']
    );
    loft49mod_extract_date_field($list, 'current');

    $prev = clone $query;
    $list['prev'] = $prev
//      ->condition('created', $node->created, '<=')
//      ->orderBy('created', 'DESC')
      ->condition('frfed.field_event_date_value', $node->field_event_date['und'][0]['value'], '<=')
      ->orderBy('frfed.field_event_date_value', 'DESC')
      ->execute()->fetchAssoc();
    
    loft49mod_extract_date_field($list, 'prev');

    $next = clone $query;
    $list['next'] = $next
//      ->condition('created', $node->created, '>=')
//      ->orderBy('created', 'ASC')
      ->condition('frfed.field_event_date_value', $node->field_event_date['und'][0]['value'], '>=')
      ->orderBy('frfed.field_event_date_value', 'ASC')
      ->execute()->fetchAssoc();
    loft49mod_extract_date_field($list, 'next');

    $last = clone $query;
    $list['last'] = $last
//      ->condition('created', $node->created, '>=')
//      ->orderBy('created', 'DESC')
      ->condition('frfed.field_event_date_value', $node->field_event_date['und'][0]['value'], '>=')
      ->orderBy('frfed.field_event_date_value', 'DESC')
      ->execute()->fetchAssoc();
    loft49mod_extract_date_field($list, 'last');
    
    $master_list[$node->nid] = $list;
  }
  return $master_list[$node->nid];
}

function loft49mod_extract_date_field(&$list, $key) {
    $fieldKey = 'field_event_date_value';
    if ($list[$key]) {
        if (function_exists('date_create_from_format')) {
            //PHP >= 5.3
            $list[$key]['date'] = DateObject::createFromFormat('Y-m-d G:i:s', $list[$key][$fieldKey])->format('D jS M');
        } else {
            //php < 5.3
            $date = new DateObject($list[$key][$fieldKey]);
            $list[$key]['date'] = $date->format('D jS M');
        }
    }
}

/**
 *  Implements template_preprocess_hook()
 */
function template_preprocess_loft49mod(&$vars) {
  drupal_add_css(drupal_get_path('module', 'loft49mod') . '/loft49mod.css');
  // for getting node type
  if ($node = menu_get_object()) {
    $vars['node'] = $node;
  }

  $label_type = variable_get('loft49mod_label_type_' . $vars['node']->type, NULL);

  if ($nav = $vars['list']) {
    if (variable_get('loft49mod_firstlast_'. $vars['node']->type, NULL)) {
      $vars['first_link'] = empty($nav['first']) ? '' : l('« ' . t(variable_get('loft49mod_first_label_' . $vars['node']->type, NULL)), 'node/' . $nav['first']['nid']);
      $vars['last_link'] = empty($nav['last']) ? '' : l(t(variable_get('loft49mod_last_label_' . $vars['node']->type, NULL)) . ' »', 'node/' . $nav['last']['nid']);
    }

    switch ($label_type) {
      case '0':
        $vars['previous_link'] = empty($nav['prev']) ? '' : l('‹ ' . t(variable_get('loft49mod_prev_label_' . $vars['node']->type, NULL)), 'node/' . $nav['prev']['nid']);
        $vars['next_link'] = empty($nav['next']) ? '' : l(t(variable_get('loft49mod_next_label_' . $vars['node']->type, NULL)) . ' ›', 'node/' . $nav['next']['nid']);
        break;
      case '1':
        $vars['previous_link'] = empty($nav['prev']) ? '' : l('<span class="dir">'.t('Previous: ').'</span> <span class="title">' . t($nav['prev']['title']) . '</span>', 'node/' . $nav['prev']['nid'], array('html'=>true, 'attributes'=>array('title'=>t($nav['prev']['title']))));
        $vars['next_link'] = empty($nav['next']) ? '' : l('<span class="dir">'.t('Next: ').'</span> <span class="title">' . t($nav['next']['title']) . '</span>', 'node/' . $nav['next']['nid'], array('html'=>true, 'attributes'=>array('title'=>t($nav['next']['title']))));
        break;
      case '2':
        $vars['previous_link'] = empty($nav['prev']) ? '' : l('‹ ' . t($nav['prev']['nid']), 'node/' . $nav['prev']['nid']);
        $vars['next_link'] = empty($nav['next']) ? '' : l(t($nav['next']['nid']) . ' gdfgdgdfgdfgdgdfgd', 'node/' . $nav['next']['nid']);
        break;
    }
  }
  $vars = array_merge($vars, $vars['list']);
  unset($vars['list']);
  unset($vars['node']);
}
