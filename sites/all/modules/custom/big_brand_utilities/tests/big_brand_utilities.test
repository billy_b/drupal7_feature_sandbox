<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 03/09/2016
 * Time: 12:23
 */
class BigBrandUtilitiesTestCase extends DrupalWebTestCase {
    /**
     * Implements getInfo().
     */
    public static function getInfo() {
        return [
            'name'        => t('Big Brand Utilities'),
            'description' => t('Test the Big Brand Utilities module.'),
            'group'       => t('Big Brand Ideas'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp() {
        parent::setUp(['big_brand_utilities']);
    }

    public function testEmptyString() {
        $this->performATest('', '', 'When filtering an empty string, should return an empty string');
    }

    public function testSingleParagraphWithSimpleLink() {
        $this->performATest(
            '<p class="text-muted"><a href="#blah">This is a link</a></p>',
            '<a href="#blah">This is a link</a>',
            'When filtering a single paragraph with a single child link, should return the child link'
        );
    }

    public function testSingleParagraphWithComplexLink() {
        $this->performATest(
            '<p class="text-muted"><a href="#blah"><span class="a-class">This is a link</span></a></p>',
            '<a href="#blah"><span class="a-class">This is a link</span></a>',
            'When filtering a single paragraph with a single child link, should return the child link and it\'s children'
        );
    }

    public function testSingleParagraphWithMultipleChildren() {
        $this->performATest(
            '<p class="text-muted"><a href="#blah"><span class="a-class">This is a link</span></a><a href="#blah"><span class="a-class">This is a link</span></a></p>',
            '<p class="text-muted"><a href="#blah"><span class="a-class">This is a link</span></a><a href="#blah"><span class="a-class">This is a link</span></a></p>',
            'When filtering a single paragraph with multiple children, return the original string'
        );
    }

    public function testTwoDifferentParagraphs() {
        $this->performATest(
            '<p>Text</p><p><a href="#blah">Text Link</a></p>',
            '<p>Text</p><a href="#blah">Text Link</a>',
            'Should only remove p tags which contain one a tag'
        );
    }

    public function testTwoSimilarParagraphs() {
        $this->performATest(
            '<p><a href="#blah"><span class="a-class">This is a link</span></a></p>
             <p><a href="#blah"><span class="a-class">This is a link</span></a></p>',
            '<a href="#blah"><span class="a-class">This is a link</span></a>
             <a href="#blah"><span class="a-class">This is a link</span></a>',
            'When filtering multiple parent tags, filter each paragraph in turn'
        );
    }

    public function testTwoSimilarParagraphsAndOneWithMultipleChildren() {
        $this->performATest(
            '<p><a href="#blah"><span class="a-class">This is a link</span></a></p>
            <p><a href="#blah"><span class="a-class">This is a link</span></a><a href="#blah"><span class="a-class">This is a link</span></a></p>
             <p><a href="#blah"><span class="a-class">This is a link</span></a></p>',
            '<a href="#blah"><span class="a-class">This is a link</span></a>
            <p><a href="#blah"><span class="a-class">This is a link</span></a><a href="#blah"><span class="a-class">This is a link</span></a></p>
             <a href="#blah"><span class="a-class">This is a link</span></a>',
            'When filtering multiple parent tags, filter each paragraph in turn'
        );
    }

    public function testFilteringUppercaseTags() {
        $this->performATest(
            '<P CLASS="text-muted"><A HREF="#blah">This is a link</A></P>',
            '<A HREF="#blah">This is a link</A>',
            'Filter should ignore case'
        );
    }

    public function testNonParagraphTags() {
        $this->performATest(
            '<pre class="text-muted"><a href="#blah">This is a link</a></pre>',
            '<pre class="text-muted"><a href="#blah">This is a link</a></pre>',
            'Should only remove paragraph tags'
        );
    }

    public function testSingleParagraphWithOtherTag() {
        $this->performATest(
            '<p class="text-muted"><pre>some pre-formatted text</pre></p>',
            '<p class="text-muted"><pre>some pre-formatted text</pre></p>',
            'Should not filter when parent paragraph contains any single child tag other than a'
        );
    }

    public function testH2FollowedByParagraph() {
        $this->performATest(
            '<h2>this is the heading</h2>
            <p><a class="btn btn-primary" href="/how-it-works">How it works</a></p>',
            '<h2>this is the heading</h2>
            <a class="btn btn-primary" href="/how-it-works">How it works</a>',
            'Should remove paragraph tags when there is a heading followed by a paragraph'
        );
    }

    private function performATest($input, $expectedOutput, $messages = '', $group = '') {
        $filter = (object) ['name' => 'big_brand_utilities_sng_p_filter'];
        $output = big_brand_utilities_filter_process($input, $filter, new stdClass(), LANGUAGE_NONE, true, '');
        $this->assertEqual($expectedOutput, $output, $messages, $group);
    }

}
