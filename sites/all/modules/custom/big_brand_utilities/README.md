# Big Brand Utilities

Implements a text filter for Drupal 7. Install the module as normal then under Administration >> Configuration >>
 Content authoring >> Text formats, click configure for each text format, and enable the filter called 
 "Single Paragraph Filter".
 
Now, when the text format is used, this filter will be in effect. It targets text content where there is a single
 paragraph tag with a single child element. In that instance, it removes the parent paragraph tag leaving the child
 tag intact. In all other cases, the original text is returned unfiltered.
