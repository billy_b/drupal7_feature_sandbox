/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

	'use strict';
	
	(function ($, Drupal, window, document, undefined) {
	
		$.browser = {};
		(function () {
			$.browser.msie = false;
			$.browser.version = 0;
			if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
				$.browser.msie = true;
				$.browser.version = RegExp.$1;
			}
		})();
	
		function checkFields(fields) {
			fields.each(function () {
				if ($(this).val() != '') {
					$(this).parents('.form-item').addClass('keyed');
				} else {
					$(this).parents('.form-item').removeClass('keyed');
				}
			});
		};
	
		/*
	  * Responsive Breakpoints
	  *
	  * $(window).responsiveBreakpoints({
	  *   distinct: false,
	  *   breakpoints: [0,768,980]
	  * });
	  */
		$.fn.responsiveBreakpoints = function (settings) {
			window.currentBreakpoint = -1;
	
			function checkBreakpoint() {
	
				var w = $(window).width();
				var lastSize,
				    newBreakpoint = -1;
				var options = jQuery.extend({
					distinct: true,
					breakpoints: new Array(768, 980, 1200)
				}, settings);
	
				for (var bp in options.breakpoints) {
					bpw = options.breakpoints[bp];
					if (w != lastSize && w >= bpw) {
						newBreakpoint = bpw;
					}
				}
	
				if (newBreakpoint != window.currentBreakpoint) {
					previousBreakpoint = window.currentBreakpoint;
					window.currentBreakpoint = newBreakpoint;
					if ($.isFunction(window['changeBreakpoint'])) {
						changeBreakpoint(previousBreakpoint, window.currentBreakpoint);
					}
				}
				lastSize = w;
			}
	
			// Set initial breakpoint
			checkBreakpoint();
	
			interval = setInterval(function () {
				// Poll for window size changes
				checkBreakpoint();
			}, 250);
		};
	
		/* Paul's field label thingy */
		$.fn.textfieldLabel = function () {
			this.each(function () {
				$(this).hide();
				var lt = $(this).text(); // label text
				var f = $('#' + $(this).attr('for')); // field element
	
	
				f.val(lt);
				f.addClass('with-label');
	
				f.focus(function () {
					if ($(this).val() == lt) {
						$(this).val('');
						$(this).removeClass('with-label');
					}
				});
	
				f.blur(function () {
					if ($(this).val() == '') {
						$(this).val(lt);
						$(this).addClass('with-label');
					}
				});
			});
		};
	
		/* Paul's clickable parent thingy */
		$.fn.clickableParent = function () {
			var iOS = navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false;
	
			if (!iOS) {
				$(this).addClass('clickable');
				this.hover(function () {
					$(this).addClass('hover');
				}, function () {
					$(this).removeClass('hover');
				});
				this.click(function (e) {
					if ($(e.target).attr('target') === '_blank') {
						return true;
					}
	
					var $anchor = $(this).find('a:first');
	
					if ($anchor.attr('target') === '_blank') {
						window.open($anchor.attr('href'), '_blank');
					} else {
						window.location = $anchor.attr('href');
					}
				});
			}
		};
	
		// when doc ready
		$(function () {
	
			// JS class
			$('html').removeClass('no-js').addClass('js');
	
			// make admin menu shortcuts visible by default
			$('#admin-menu .shortcut-toolbar').addClass('active');
	
			$('#block-views-galleries-block .view-galleries.view-display-id-block .view-content .views-row ul').cycle({
				fx: 'scrollDown',
				speed: 300,
				timeout: 8000
			});
	
			// jquery cycle gallery view http://jquery.malsup.com/cycle/ 
			$('.view-galleries.view-display-id-page .view-content .views-row .views-field-field-photos.gallery-pics ul').cycle();
			// gallery view
			$('.view-galleries.view-display-id-page .view-content .views-row .views-field-field-photos.gallery-pics ul').cycle();
	
			$('#block-views-galleries-block h2.block-title').prepend('<i class="fa fa-camera-retro" style="padding-right: 5px; "></i>');
	
			$('.view-calendar.view-display-id-page_1 .view-content .views-row .views-field-field-event-image ul, .view-calendar.view-display-id-page_2 .view-content .views-row .views-field-field-event-image ul, .view-calendar.view-display-id-page_3 .view-content .views-row .views-field-field-event-image ul').cycle();
	
			$('#block-views-feature-boxes-block .view-feature-boxes.view-display-id-block .views-row').clickableParent();
			$('.view-galleries.view-display-id-page .view-content .views-row').clickableParent();
	
			// Broad checkfields label js
			// checkFields($('input, textarea', 'form.webform-client-form'));
	
			// $('input, textarea', 'form.webform-client-form').on('focus', function() {
			//        $(this).parents('.form-item').addClass('focussed');
			// });
			// $('input, textarea', 'form.webform-client-form').on('blur', function() {
			//        $(this).parents('.form-item').removeClass('focussed');
			// });
			// $('input, textarea', 'form.webform-client-form').on('keyup', function() {
			//        checkFields($(this));
			// });
	
	
			var $isocontainer = $('#isotope-masonry');
			$isocontainer.imagesLoaded(function () {
				$isocontainer.isotope({
	
					masonry: {
						columnWidth: 5
					}
				});
			});
	
			var $isocontainerprojs = $('#isotope-masonry-projects');
			$isocontainerprojs.imagesLoaded(function () {
				$isocontainerprojs.isotope({
	
					masonry: {
						columnWidth: 1
					}
				});
			});
	
			// http://www.aaronvanderzwan.com/maximage/
	
			$('body.node-type-project #maximage').maximage({
				cycleOptions: {
					fx: 'fade',
					// Speed has to match the speed for CSS transitions
					speed: 2500,
					timeout: 3500,
					prev: '#arrow_left',
					next: '#arrow_right'
				}
			});
	
			// Error red color on reqd textfields
			$('form.webform-client-form .form-item input').each(function (index, value) {
				// console.log(index, value);
				if ($(this).hasClass('error')) {
					$(this).parent().addClass('required').addClass('error');
				};
			});
	
			$('#block-custom-search-blocks-1').prepend('<a href="#" class="close-search" title="Close"><i class="fa fa-times-circle-o"></i></a>');
	
			$('#block-custom-search-blocks-1 a.close-search').on('click', function (e) {
				$('.search-link-holder a.search-link').toggleClass('search-on');
				$('#block-custom-search-blocks-1').toggleClass('search-box-on');
				$('body').toggleClass('search-mask-on');
			});
	
			$('.search-link-holder a.search-link').on('click', function (e) {
				$(this).toggleClass('search-on');
				$('#block-custom-search-blocks-1').toggleClass('search-box-on');
				$('body').toggleClass('search-mask-on');
			});
	
			// Textfields / areas checkfields label js
			checkFields($('.webform-client-form .form-item.webform-component-textfield input, .webform-client-form textarea'));
	
			$('.webform-client-form .form-item.webform-component-textfield input, .webform-client-form textarea').on('focus', function () {
				$(this).parents('.form-item').addClass('focussed');
			});
			$('.webform-client-form .form-item.webform-component-textfield input, .webform-client-form textarea').on('blur', function () {
				$(this).parents('.form-item').removeClass('focussed');
			});
			$('.webform-client-form .form-item.webform-component-textfield input, .webform-client-form textarea').on('keyup', function () {
				checkFields($(this));
			});
	
			checkFields($('.webform-client-form .form-item.webform-component-email input'));
	
			$('.webform-client-form .form-item.webform-component-email input').on('focus', function () {
				$(this).parents('.form-item').addClass('focussed');
			});
			$('.webform-client-form .form-item.webform-component-email input').on('blur', function () {
				$(this).parents('.form-item').removeClass('focussed');
			});
			$('.webform-client-form .form-item.webform-component-email input').on('keyup', function () {
				checkFields($(this));
			});
	
			// Search block checkfields label js
			checkFields($('.search-form#custom-search-blocks-form-1 input, .search-form#custom-search-blocks-form-1 textarea'));
	
			$('.search-form#custom-search-blocks-form-1 input, .search-form#custom-search-blocks-form-1 textarea').on('focus', function () {
				$(this).parents('.form-item').addClass('focussed');
			});
			$('.search-form#custom-search-blocks-form-1 input, .search-form#custom-search-blocks-form-1 textarea').on('blur', function () {
				$(this).parents('.form-item').removeClass('focussed');
			});
			$('.search-form#custom-search-blocks-form-1 input, .search-form#custom-search-blocks-form-1 textarea').on('keyup', function () {
				checkFields($(this));
			});
	
			// Error red color on reqd textfields
			$('form.webform-client-form .form-item input').each(function (index, value) {
				// console.log(index, value);
				if ($(this).hasClass('error')) {
					$(this).parent().addClass('required').addClass('error');
				};
			});
	
			// Flexslider length check - hide arrows if only one
	
			if ($('#flexslider-1 .slides > li').length >= 2) {} else {
				$('#flexslider-1 .flex-direction-nav').hide();
			};
	
			if ($('#flexslider-3 .slides > li').length >= 2) {} else {
				$('#flexslider-3 .flex-direction-nav').hide();
			};
	
			// spotlight empty fields hide
			$('#block-views-spotlight-block-2 .view-display-id-block_2 .view-content #spot-mains.flexslider ul.slides li').each(function (index, value) {
				var btitle = $(this).find('.views-field-nothing .views-field-title h3').text().length;
				var bbody = $(this).find('.views-field-nothing .spotlight-body p').text().length;
				var blink = $(this).find('.views-field-nothing .spotlight-link a').text().length;
	
				if (btitle == 0 && bbody == 0 && blink == 0) {
					$(this).find('.views-field-nothing').hide();
				};
			});
	
			// spotlight empty fields hide
			$('#block-views-spotlight-block-2 .view-display-id-block_2 .view-content .owl-carousel .owl-wrapper .owl-item').each(function (index, value) {
				var btitle = $(this).find('.views-field-nothing .views-field-title h3').text().length;
				var bbody = $(this).find('.views-field-nothing .spotlight-body p').text().length;
				var blink = $(this).find('.views-field-nothing .spotlight-link a').text().length;
	
				if (btitle == 0 && bbody == 0 && blink == 0) {
					$(this).find('.views-field-nothing').hide();
				};
			});
	
			// if ($('#block-views-spotlight-block-2 .view-display-id-block_2 .view-content #spot-mains.flexslider ul.slides li .views-field-nothing .views-field-title h3')) {
	
	
			// };
	
	
			// spotlight arrow
			$('#block-views-spotlight-block-2 .view-display-id-block_2 .view-content .owl-carousel .owl-wrapper-outer .owl-item .views-field-nothing .spotlight-link a').append('<i class="fa fa-angle-right" style="padding-left: 5px; "></i>');
	
			$('.region-sidebar-second #block-views-calendar-block-2.block h2.block-title a.block-title-link').prepend('<i class="fa fa-calendar" style="padding-right: 5px; "></i>');
	
			$('#block-views-testimonials-block h2.block-title').prepend('<i class="fa fa-comments" style="padding-right: 8px; "></i>');
			$('#block-views-calendar-block-2 .view-calendar.view-display-id-block_2 .more-link a').append('<i class="fa fa-angle-right" style="padding-left: 5px; "></i>');
	
			// Drupal.behaviors.CVBuilder = {
			// attach: function(context){
			// 	console.log('CVBuilder attached', context);
	
	
			// 		// Broad checkfields label js
			// 		checkFields($('.webform-client-form input, .webform-client-form textarea'));
	
			// 		$('.webform-client-form input, .webform-client-form textarea').on('focus', function() {
			// 		       $(this).parents('.form-item').addClass('focussed');
			// 		});
			// 		$('.webform-client-form input, .webform-client-form textarea').on('blur', function() {
			// 		       $(this).parents('.form-item').removeClass('focussed');
			// 		});
			// 		$('.webform-client-form input, .webform-client-form textarea').on('keyup', function() {
			// 		       checkFields($(this));
			// 		});
	
	
			// 	}
			// };
	
	
			// if $('.container.fwc .row.fwr .region-contenttop-fullwidth #block-views-feature_boxes-block .block-container .content .view-feature-boxes.view-id-feature_boxes.view-display-id-block .view-content .node-feature-box').hasClass('hover') {
	
			// };
	
	
			// $.simpleWeather({
			//   location: 'Manchester, UK',
			//   woeid: '',
			//   unit: 'c',
			//   success: function(weather) {
			//     html = '<h2><i class="icon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+'</h2>';
			//     html += '<ul><li>'+weather.city+', '+weather.region+'</li>';
			//     html += '<li class="currently">'+weather.currently+'</li>';
			//     html += '</ul>';
	
			//     $("#weather").html(html);
			//   },
			//   error: function(error) {
			//     $("#weather").html('<p>'+error+'</p>');
			//   }
			// });
	
	
			$('#search-form .search-advanced .panel-heading a.panel-title').addClass('collapsed');
	
			//$('#block-views-feature-boxes-block .view-feature-boxes.view-display-id-block .views-row .third-width-feature .field-name-field-feature-box-link a').append('<i class="fa fa-angle-right" style="padding-left: 5px; "></i>');
			$('.view-courses.view-display-id-block .views-row .views-field-title h4 a').append('<i class="fa fa-angle-right" style="padding-left: 5px; "></i>');
	
			// $('.flexslider .flex-direction-nav a').append('<i class="fa fa-angle-right" style="padding-left: 5px; "></i>');
	
			$('#block-views-tweets-block h2.block-title a').prepend('<i class="fa fa-twitter" style="padding-right: 8px;"></i>');
			$('#block-block-5 h2.block-title a').prepend('<i class="fa fa-facebook" style="padding-right: 8px;"></i>');
			$('.highlighted.jumbotron #block-views-spotlight-block .view-spotlight.view-display-id-block .view-content #owl-carousel-block4 .owl-wrapper-outer .owl-item .views-field-field-highlight-link h3 a').append('<i class="fa fa-angle-right" style="padding-left: 5px; "></i>');
			$('#block-views-tweets-block .view-tweets .more-link a').append('<i class="fa fa-angle-right" style="padding-left: 1px; "></i>');
			$('#block-views-spotlight-block-2 .view-display-id-block_2 .view-content #spot-mains.flexslider ul.slides li .views-field-nothing .spotlight-link a').append('<i class="fa fa-chevron-right" style="padding-left: 5px; "></i>');
			$('.view-courses.view-display-id-block_1 .views-row .views-field-view-node a').append('<i class="fa fa-angle-right" style="padding-left: 5px; "></i>');
			$('#block-views-related-content-block .view-related-content.view-display-id-block .views-row .views-field-view-node a').append('<i class="fa fa-angle-right" style="padding-left: 5px; "></i>');
			$('.masonry-holder.field-name-field-masonry-selection #isotope-masonry .brick .node-masonry-feature-box .field-name-field-masonry-link a').append('<i class="fa fa-angle-right" style="padding-left: 5px; "></i>');
	
			$('#block-views-feature-boxes-block .view-feature-boxes.view-display-id-block .views-row .full-width-feature .field-name-field-feature-box-link a, #block-views-feature-boxes-block .view-feature-boxes.view-display-id-block .views-row .half-width-feature .field-name-field-feature-box-link a, #block-views-feature-boxes-block .view-feature-boxes.view-display-id-block .views-row .quarter-width-feature .field-name-field-feature-box-link a, #block-views-feature-boxes-block .view-feature-boxes.view-display-id-block .views-row .third-width-feature .field-name-field-feature-box-link a').append('<i class="fa fa-angle-right" style="padding-left: 5px; "></i>');
	
			// $('a.pdf-download-button').append('<i class="fa fa-angle-right" style="padding-left: 10px; "></i>');
			$('a.pdf-download-button').prepend('<i class="fa fa-file-pdf-o fa-6" style="padding-right: 10px; "></i>');
			$('a.left-link-button').prepend('<i class="fa fa-info-circle fa-6" style="padding-right: 10px; "></i>');
	
			$('a.link-button').append('<i class="fa fa-angle-right" style="padding-left: 10px; "></i>');
			$('#block-block-10 a.link-button').prepend('<i class="fa fa-info-circle" style="padding-right: 10px; "></i>');
	
			// if ($('.view-sidebar-page-content.view-display-id-block_2 .view-content .file-video .pic-caption, .view-sidebar-page-content.view-display-id-block_2 .view-content .views-field-field-right-sidebar-pics-1 .pic-caption, .view-sidebar-page-content.view-display-id-block_2 .view-content .views-field-field-right-sidebar-pics .pic-caption, .view-sidebar-page-content.view-display-id-block .view-content .file-video .pic-caption, .view-sidebar-page-content.view-display-id-block .view-content .views-field-field-right-sidebar-pics-1 .pic-caption, .view-sidebar-page-content.view-display-id-block .view-content .views-field-field-right-sidebar-pics .pic-caption').length > 1) {
			// 	alert('pc');
			// };
	
	
			$('.view-sidebar-page-content.view-display-id-block_2 .view-content .file-video .pic-caption, .view-sidebar-page-content.view-display-id-block_2 .view-content .views-field-field-right-sidebar-pics-1 .pic-caption, .view-sidebar-page-content.view-display-id-block_2 .view-content .views-field-field-right-sidebar-pics .pic-caption, .view-sidebar-page-content.view-display-id-block .view-content .file-video .pic-caption, .view-sidebar-page-content.view-display-id-block .view-content .views-field-field-right-sidebar-pics-1 .pic-caption, .view-sidebar-page-content.view-display-id-block .view-content .views-field-field-right-sidebar-pics .pic-caption').each(function (index) {
	
				if ($(this).text().length > 1) {
	
					$(this).addClass('w-content');
				};
			});
	
			if ($('body').hasClass('page-node-5') || $('body').hasClass('page-node-106')) {
	
				//Google Map Skin - Get more at http://snazzymaps.com/ https://snazzymaps.com/style/30/cobalt
				var myOptions = {
					zoom: 13,
					center: new google.maps.LatLng(51.498360, -0.149137),
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					// disableDefaultUI: true,
	
					styles: [{ "featureType": "administrative", "elementType": "all", "stylers": [{ "visibility": "on" }, { "lightness": 33 }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#f2e5d4" }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#c5dac6" }] }, { "featureType": "poi.park", "elementType": "labels", "stylers": [{ "visibility": "on" }, { "lightness": 20 }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry", "stylers": [{ "color": "#c5c6c6" }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#e4d7c6" }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#fbfaf7" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "visibility": "on" }, { "color": "#acbcc9" }] }]
				};
	
				// Load the map into the Drop down - otherwise map wont display properly
				var map = new google.maps.Map(document.getElementById('contact-map'), myOptions);
	
				// setTimeout(function(){
				//        $('.region-top-popout').prepend($("#contact-map").css('background', 'gray').get(0));
				//    }, 500);
	
				var marker = new google.maps.Marker({
					position: myOptions.center,
					map: map
				});
	
				marker.setIcon('/sites/all/themes/knightsbridge/images/map-marker.png');
			};
		});
		/* End Doc Ready */
	
		/* End Compat */
	
		// Drupal.behaviors.CVBuilder = {
		// 	attach: function(context){
		// 		console.log('CVBuilder attached', context);
	
	
		// 		// Broad checkfields label js
		// 		checkFields($('.webform-client-form-94 input, .webform-client-form-94 textarea'));
	
		// 		$('.webform-client-form-94 input, .webform-client-form-94 textarea').on('focus', function() {
		// 		       $(this).parents('.form-item').addClass('focussed');
		// 		});
		// 		$('.webform-client-form-94 input, .webform-client-form-94 textarea').on('blur', function() {
		// 		       $(this).parents('.form-item').removeClass('focussed');
		// 		});
		// 		$('.webform-client-form-94 input, .webform-client-form-94 textarea').on('keyup', function() {
		// 		       checkFields($(this));
		// 		});
	
	
		// 	}
		// };
	
	})(jQuery, Drupal, undefined, undefined.document);

/***/ }
/******/ ]);
//# sourceMappingURL=project.webpacked.js.map