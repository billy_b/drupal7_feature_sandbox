function myModuleTwo() {
	this.start = function() {
		return 'start!';
	}

	this.finish = function() {
		return 'finish';	
	}
}

module.exports = myModuleTwo;