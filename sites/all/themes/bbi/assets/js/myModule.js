function myModule() {
	this.hello = function() {
		return 'hello!';
	}

	this.goodbye = function() {
		return 'goodbye!!!';
	}

	this.testOfDefaultParams = function(str = 'local') {
		return str;
	}
}

module.exports = myModule;