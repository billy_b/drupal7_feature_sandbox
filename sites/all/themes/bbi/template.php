<?php



/**
 * Implements template_preprocess_node()
 */
function bbi_preprocess_node(&$variables) {
	// Example templates:
	// node--product--full.tpl.php
	// node--product--teaser.tpl.php
	if (isset($variables['node']->type) && isset($variables['view_mode'])) {
    $variables['theme_hook_suggestions'][] = 'node__'.$variables['node']->type.'__'.$variables['view_mode'];
  }
}


// Add Bootstrap classes to menu block

function bbi_menu_tree($variables) {
  return '<ul class="menu nav navbar-nav">' . $variables['tree'] . '</ul>';
}


// Devel DPM
// function dpm($input, $name = NULL, $type = 'status') {
//   if (user_access('access devel information')) {
//     $export = kprint_r($input, TRUE, $name);
//     drupal_set_message($export, $type);
//   }
//   return $input;
// }

function bbi_preprocess_html(&$variables) {
	$body_classes = array($variables['classes_array']);
	if ($variables['user']) {
		foreach($variables['user']->roles as $key => $role){
			$role_class = 'role-' . str_replace(' ', '-', $role);
			$variables['classes_array'][] = $role_class;
		}
	}
}

function bbi_alpha_preprocess_node(&$variables) {
    if ($variables['view_mode'] != 'teaser') {
      $variables['attributes_array']['class'][] = 'node-' . $variables['view_mode'];
  }
}







/**
 * Implements hook_form_FORM_ID_alter() for search_form().
 */
function bbi_form_search_form_alter(&$form, &$form_state) {
  // Add a clearfix class so the results don't overflow onto the form.
  $form['#attributes']['class'][] = 'clearfix';

  // Remove container-inline from the container classes.
  $form['basic']['#attributes']['class'] = array();

  // Hide the default button from display.
  $form['basic']['submit']['#attributes']['class'][] = 'element-invisible';

  // Implement a theme wrapper to add a submit button containing a search
  // icon directly after the input element.
  $form['basic']['keys']['#theme_wrappers'] = array('search_form_wrapper');
  $form['basic']['keys']['#title'] = '';
  $form['basic']['keys']['#attributes']['placeholder'] = t('Search');
  $form['basic']['keys']['#attributes']['class'][] = 'form-control';
}


/**
 * Implements hook_form_FORM_ID_alter() for search_block_form().
 */
function bbi_form_search_block_form_alter(&$form, &$form_state) {
  $form['#attributes']['class'][] = 'form-search';

  $form['search_block_form']['#title'] = '';
  $form['search_block_form']['#attributes']['placeholder'] = t('Search');
  $form['search_block_form']['#attributes']['class'][] = 'form-control';

  // Hide the default button from display and implement a theme wrapper
  // to add a submit button containing a search icon directly after the
  // input element.
  $form['actions']['submit']['#attributes']['class'][] = 'element-invisible';
  $form['search_block_form']['#theme_wrappers'] = array('search_form_wrapper');

  // Apply a clearfix so the results don't overflow onto the form.
  $form['#attributes']['class'][] = 'content-search';
}

/**
 * Implements theme_search_form_wrapper()
 */
function bbi_search_form_wrapper($variables) {
 
  $output = $variables['element']['#children'];
  $output .= '<button type="submit" class="btn btn-default search-submit"><i class="fa fa-search"></i></button>';
  
  return $output;
}

/**
 * Implements hook_theme()
 * Our custom theme hooks wont work unless we declare them here.
 */
function bbi_theme(&$existing, $type, $theme, $path) {
  $hook_theme = array(
    'search_form_wrapper' => array(
      'render element' => 'element',
    )
  
	);
	
	return $hook_theme;
}



function bbi_form_system_site_information_settings_alter(&$form, &$form_state)  {
  $form['phone_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone Number'),
    '#description' => t('Appears in the website footer'),
    '#default_value' => variable_get('phone_number', ''),
  );
}


