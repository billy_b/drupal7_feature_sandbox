<header class="navbar-fixed-top header" id="top">
  <div class="container">
    <div class="flex flex--jcsb"> <a class="navbar-brand" href="/" title="PCT"> <img src="assets/images/svg/pct-logo.svg" alt="" class="logo-black logo-svg"> <img src="assets/images/pct-logo-white.png" alt="" class="logo-white"> </a> 
      
      <!-- Static navbar -->
      <nav class="navbar navbar-default navbar-primary"> 
        <!-- <div class="container-fluid"> -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav fs-m">
            <li class="home"><a href="/">Home</a></li>
            <li class="solutions"><a href="#">Solutions</a></li>
            <li class="why"><a href="#">Why PCT</a></li>
            <li class="about"><a href="/about.php">About us</a></li>
            <li class="article"><a href="article.php">News</a></li>
            <li class="contact"><a href="contact.php">Contact us</a></li>
          </ul>
        </div>
        <!--/.nav-collapse --> 
        <!-- </div> --><!--/.container-fluid --> 
      </nav>
    </div>
  </div>
</header>