<footer class="footer footer--menu ptb-l bgc-dark-blue">
  <div class="container mtb-m">
    <div class="row flex">
      <div class="col-md-4 flex flex__item-one-third" id="subscribe">
        <p class="mb-m fs-m fc-white lhl">We're here to help if you want to talk to us about our products and services, or if you have a specific query.</p>
        <h4 class="col-md-12 p-n fs-xl fc-white"> <a href="mailto:partnersales@paymentct.com" class="fc-pink fw-b">partnersales@paymentct.com</a> <br>
          or call&nbsp; <a href="tel:+44 (0)203 3971699" class="fc-pink fw-b">+44 (0)203 3971699</a> </h4>
      </div>
      <div class="col-md-8 flex ptb-s">
        <ul class="social-nav social-nav--footer no-list nav navbar-nav flex flex--jcfe">
          <li class="social-nav__list-item"> <a class="social-nav__link" href="" title="See us on Vimeo" target="_blank">
            <svg xmlns="http://www.w3.org/2000/svg" class="social-nav__icon social-nav__icon--google" version="1.1">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-vimeo"></use>
            </svg>
            </a> </li>
          <li class="social-nav__list-item"> <a class="social-nav__link" href="" title="Follow us on Facebook" target="_blank">
            <svg xmlns="http://www.w3.org/2000/svg" class="social-nav__icon social-nav__icon--fb" version="1.1">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use>
            </svg>
            </a> </li>
          <li class="social-nav__list-item"> <a class="social-nav__link" href="" title="Follow us on Twitter" target="_blank">
            <svg xmlns="http://www.w3.org/2000/svg" class="social-nav__icon social-nav__icon--twitter" version="1.1">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-twitter"></use>
            </svg>
            </a> </li>
          <li class="social-nav__list-item"> <a class="social-nav__link" href="" title="Follow us on Google+" target="_blank">
            <svg xmlns="http://www.w3.org/2000/svg" class="social-nav__icon social-nav__icon--google" version="1.1">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-google"></use>
            </svg>
            </a> </li>
          <li class="social-nav__list-item"> <a class="social-nav__link" href="" title="Follow us on Instagram" target="_blank">
            <svg xmlns="http://www.w3.org/2000/svg" class="social-nav__icon social-nav__icon--google" version="1.1">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-instagram"></use>
            </svg>
            </a> </li>
        </ul>
        <div class="logoset logoset--footer col-md-12 text-right flex flex--aife flex--jcfe"> <a href="#" class="logoset__item m-s mb-n"> <img src="/assets/images/logos/logo-wirecard-wh.png" alt=""> </a> <a href="#" class="logoset__item m-s mb-n"> <img src="/assets/images/logos/logo-mastercard-wh.png" alt=""> </a> <a href="#" class="logoset__item m-s mb-n"> <img src="/assets/images/logos/logo-unisys-wh.png" alt=""> </a> <a href="#" class="logoset__item m-s mb-n"> <img src="/assets/images/logos/logo-visa-wh.png" alt=""> </a> <a href="#" class="logoset__item m-s mb-n"> <img src="/assets/images/logos/logo-raphaels_bank-wh.png" alt=""> </a> </div>
      </div>
    </div>
  </div>
</footer>