module.exports = function(grunt) {

	'use strict';

	var config = {

		sass: {

			live: {
				options: {
					// style: 'compressed',
					sourcemap: 'file'
					
				},

				files: {
					'assets/css/combined.min.css': 'assets/scss/main.scss'
				}
			}
		},

		cssmin: {

			options: {
				sourceMap: true
			},

			live: {
				files: {
					'assets/css/combined.min.css': [
						'assets/css/combined.min.css'
					]
				}
			}
		},

		uglify : {

			dev: {
				options: {
					mangle : true,
					preserveComments: false,
					sourceMap: true
				},

				files : {

					// Main application
					'assets/js/combined.min.js' : 'assets/js/project.webpacked.js'
				}
			}
		},

		webpack: {
			someName: {
				entry: "./assets/js/project.js",
				output: {
					path: "./assets/js/",
					filename: "project.webpacked.js",
				},

				devtool: 'source-map',
				watch: true,
				keepalive: true,

				module: {
					loaders: [
						{
							test: /\.js$/,
						    exclude: /(node_modules)/,
						    loader: 'babel',
						    query: {
						    	presets: ['es2015']
						    }
						}
					]
				}
			}
		},

		smushit: {
			live: {
				src: [
					'assets/images/{,*/}*.{png,jpg,gif}'
				]
			}
		},

		autoprefixer: {
            dist: {
                files: {
                    'assets/css/combined.min.css': 'assets/css/combined.min.css'
                }
            },
            options: {
	        	browsers: ['> 1%', 'last 4 versions', 'Firefox ESR', 'Opera 12.1']
		    }
        },

		svgstore: {
			options: {
				prefix : 'icon-', // This will prefix each <g> ID
			},
			default : {
				files: {
					'assets/images/svg-defs.svg': ['assets/images/svg/*.svg'],
				}
			}
		},

		watch: {

			sass: {
				files: ['assets/scss/{,*/}{,*/}{,*/}*.scss'],
				tasks: ['sass:live', 'autoprefixer']
			},

			svg: {
				files: ['assets/images/svg/*.svg'],
				tasks: ['svgstore'],

				options: {
					livereload: true
				}
			},

			css: {
				files: ['assets/css/*.css', '!assets/css/*.min.css'],

				options: {
					livereload: true
				}
			},

			config: {
				files: ['Gruntfile.js'],

				options: {
					reload: true
				}
			}
		},

		// watch and webpack have separate watchers
		// so using concurrent to run both
		concurrent: {
	        target: ['watch', 'webpack'],
			options: {
				logConcurrentOutput: true
			}
	    }
	};

	// Turn off source maps for uglify:live
	config.uglify.live = JSON.parse(JSON.stringify(config.uglify.dev));
	config.uglify.live.options.sourceMap = false;

	// Configure
	grunt.initConfig(config);

	// Load all grunt tasks
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	grunt.registerTask('images', ['smushit:live']);
	grunt.registerTask('dev', ['sass:live', 'autoprefixer', 'svgstore', 'concurrent:target']);
	grunt.registerTask('live', ['sass:live', 'uglify:live', 'cssmin:live']);

};
